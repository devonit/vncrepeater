Imported from lab/vncrepeater

# Install
Make sure you have libvncserver installed. Then run:
```
cmake .
make
```

And optionally:
```
make install
```

#Operation
First install novnc into the webclients directory:
```
cd webclients
git clone https://github.com/kanaka/noVNC.git novnc
```

Then run the application as follows:
```
./repeater <your public ip address> <timeout (in seconds)> <incoming vnc server port> <port to serve vnc> <http port>
```

Start a reverse VNC connection:
```
X11VNC_REVERSE_CONNECTION_NO_AUTH=1 x11vnc -connect <your public ip address>:<incomming vnc server port>
```

Browse to "http://<your public ip address>:<http port>" and bask in the glory of reflected VNC without java!
