cmake_minimum_required (VERSION 2.6)
project (vncrepeater)

include(FindPkgConfig)

pkg_check_modules(LIBVNCSERVER REQUIRED libvncserver)
pkg_check_modules(LIBVNCCLIENT REQUIRED libvncclient)

include_directories(${LIBVNCSERVER_INCLUDE_DIRS} ${LIBVNCCLIENT_INCLUDE_DIRS})
link_directories(${LIBVNCSERVER_LIBRARY_DIRS} ${LIBVNCCLIENT_LIBRARY_DIRS})

add_executable(vncrepeater repeater.c)
target_link_libraries(vncrepeater ${LIBVNCSERVER_LIBRARIES} ${LIBVNCCLIENT_LIBRARIES})

install(TARGETS vncrepeater DESTINATION bin)
