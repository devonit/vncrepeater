#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <syslog.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <rfb/rfbclient.h>
#include <rfb/rfb.h>

#define USECS 1000000
#define MAX_PORT 65535
#define MIN_PORT 1
#define MIN_TIMEOUT 0

static rfbBool quit = FALSE;
static rfbClient* s_client = 0;
static rfbScreenInfoPtr s_server = 0;
static int bitsPerSample = 5;
static int samplesPerPixel = 3;
static int bytesPerPixel = 2;
static int connected_clients = 0;

static rfbBool mallocFrameBuffer(rfbClient* client)
{
    if(client->frameBuffer)
        free(client->frameBuffer);

    client->frameBuffer = (uint8_t*) malloc((size_t)(uint64_t)client->width * client->height * client->format.bitsPerPixel / 8);

    if ( client->frameBuffer ) {
        rfbNewFramebuffer(s_server,
                          client->frameBuffer,
                          client->width,
                          client->height,
                          5, 3,
                          client->format.bitsPerPixel / 8);
        return TRUE;
    }

    return FALSE;
}

static void gotFrameBufferUpdate(rfbClient* client, int x, int y, int w, int h)
{
    rfbMarkRectAsModified(s_server, x, y, x + w, y + h);
}

static void clientGoneHook(rfbClientPtr cl)
{
    syslog(LOG_INFO, "a client disconnected");

    --connected_clients;

    if (connected_clients <= 0) {
        syslog(LOG_INFO, "all clients disconnected, cleaning up session");
        quit = TRUE;
    }
}

static enum rfbNewClientAction newClientHook(rfbClientPtr cl)
{
    syslog(LOG_INFO, "new client connected");

    ++connected_clients;

    cl->clientGoneHook = clientGoneHook;
    return RFB_CLIENT_ACCEPT;
}

static void keyEventHook(rfbBool down,rfbKeySym key,rfbClientPtr cl)
{
    SendKeyEvent(s_client, key, down);
}

static void mouseEventHook(int buttonMask, int x, int y, rfbClientPtr cl)
{
    SendPointerEvent(s_client, x, y, buttonMask);
}

rfbBool initialize_repeater(char *hostname, int clientPort, int serverPort)
{
    // XXX *hand wave* Pay no attention to the fake argv.
    char *fakeargvServer[] = { "spaceman, i always wanted you to go into space, man" };
    int fakeargcServer = 1;

    /* get a vnc client structure (don't connect yet). */
    s_client = rfbGetClient(bitsPerSample, samplesPerPixel, bytesPerPixel);
    s_client->listenPort = clientPort;
    s_client->listen6Port = clientPort;
    s_client->canHandleNewFBSize = TRUE;
    s_client->height = 100;
    s_client->width = 100;
    s_client->GotFrameBufferUpdate = gotFrameBufferUpdate;
    s_client->MallocFrameBuffer = mallocFrameBuffer;

    // Set client encoding
    s_client->appData.encodingsString = "tight";
    s_client->appData.qualityLevel = 7;

    s_server = rfbGetScreen(&fakeargcServer,
                            fakeargvServer,
                            s_client->height,
                            s_client->width,
                            bitsPerSample,
                            samplesPerPixel,
                            bytesPerPixel);
    if (!s_server) {
        return FALSE;
    }

    s_server->desktopName = "VNC Shadow";
    s_server->frameBuffer = s_client->frameBuffer;
    s_server->neverShared = FALSE;

    s_server->port = serverPort;
    s_server->ipv6port = serverPort;
    memcpy(s_server->thisHost, hostname, strlen(hostname) );

    s_server->newClientHook = newClientHook;
    s_server->ptrAddEvent = mouseEventHook;
    s_server->kbdAddEvent = keyEventHook;
    rfbInitServer(s_server);
    rfbLogEnable(TRUE);

    return TRUE;
}

rfbBool valid_arguments(char *hostname, char *server_port_str, int timeout, int client_port)
{
    struct addrinfo hints, *servinfo;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    int getaddrinfo_status = getaddrinfo(hostname, server_port_str, &hints, &servinfo);
    if (getaddrinfo_status != 0) {
        syslog(LOG_ERR, "invalid hostname: %s\n", gai_strerror(getaddrinfo_status));
        fprintf(stderr, "invalid hostname: %s\n", gai_strerror(getaddrinfo_status));
        return FALSE;
    } else {
        freeaddrinfo(servinfo);
    }

    if (timeout < MIN_TIMEOUT) {
        syslog(LOG_ERR, "invalid timeout: %d", timeout);
        fprintf(stderr, "invalid timeout: %d\n", timeout);
        return FALSE;
    }

    if (client_port > MAX_PORT || client_port < MIN_PORT) {
        syslog(LOG_ERR, "invalid client port: %d", client_port);
        fprintf(stderr, "invalid client port: %d\n", client_port);
        return FALSE;
    }

    int server_port = atoi(server_port_str);
    if (server_port > MAX_PORT || server_port < MIN_PORT) {
        syslog(LOG_ERR, "invalid server port: %d", server_port);
        fprintf(stderr, "invalid server port: %d\n", server_port);
        return FALSE;
    }

    return TRUE;
}

int main(int argc, char **argv) {
    openlog("vncrepeater", LOG_PID | LOG_CONS, LOG_USER);

    rfbEnableClientLogging = TRUE;

    char *fakeargvClient[] = { argv[0] };
    int fakeargcClient = 1;
    if ( argc < 5 ) {
        fprintf(stderr, "usage: vncrepeater <hostname> <timeout (in seconds)> <server port> <client port>\n");
        return 1;
    }

    char *hostName = argv[1];
    int timeout = atoi(argv[2]);
    int timeout_usecs = timeout * USECS;
    int clientPort = atoi(argv[3]);
    int serverPort = atoi(argv[4]);

    if (!valid_arguments(hostName, argv[4], timeout, clientPort)) {
        return EXIT_FAILURE;
    }

    if (!initialize_repeater(hostName, clientPort, serverPort)) {
        syslog(LOG_ERR, "could not create server");
        return EXIT_FAILURE;
    }

    rfbBool cleanupClient = FALSE;
    if (listenForIncomingConnectionsNoFork(s_client, timeout_usecs) > 0) {
        if (!rfbInitClient(s_client, &fakeargcClient, fakeargvClient)) {
            quit = TRUE;
        } else {
            cleanupClient = TRUE;
        }
    } else {
        syslog(LOG_INFO, "timing out, no connection in %d seconds", timeout);
        quit = TRUE;
    }

    rfbBool checkClientConnect = TRUE;
    time_t clientConnectStart;
    time_t clientConnectWait;
    time(&clientConnectStart);
    while (!quit) {
        int i = WaitForMessage(s_client, 5000);
        if (i < 0) {
            syslog(LOG_ERR, "bad message");
            quit=TRUE;
        } else if (i) {
            if (!HandleRFBServerMessage(s_client)) {
                syslog(LOG_ERR, "server message not handled");
                quit = TRUE;
            }
        }

        if (rfbIsActive(s_server)) {
            rfbProcessEvents(s_server, 5000);
        }

        /* XXX Timeout if there are no viewing clients connected.  This handles
         * the case where a session is started and the server has connected,
         * but we're still waiting on a viewing client connection.
         *
         * 'WaitForMessage' has a timeout value, but it seems to be unreliable.
         * Timeouts sometimes happen with values of 30 seconds, but 60 seems to
         * stay connected by way of keep alives.
         */
        if (checkClientConnect) {
            time(&clientConnectWait);
            double secondsElapsed = difftime(clientConnectWait, clientConnectStart);
            if ((secondsElapsed > timeout) &&
                (connected_clients == 0)) {
                syslog(LOG_INFO, "timing out, no clients connected after %d seconds", timeout);
                quit = TRUE;

            // Time has passed and we have clients connected. We can stop checking now.
            } else if ((secondsElapsed > timeout) &&
                       (connected_clients > 0)) {
                checkClientConnect = FALSE;
            }
        }
    }

    if (s_server->frameBuffer) {
        free(s_server->frameBuffer);
        s_server->frameBuffer = NULL;
    }

    if (cleanupClient) {
        rfbClientCleanup(s_client);
    }
    rfbScreenCleanup(s_server);
    closelog();

    return EXIT_SUCCESS;
}
